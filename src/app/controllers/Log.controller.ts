import { Inject, Service } from "typedi";
import LogServicio from "../services/log/Log.service";
import { Request, Response } from "express";

@Service()
export default class LogControlador {
  @Inject(type => LogServicio)
  logServicio: LogServicio;

  public crearRegistro = async (req: Request, res: Response) => {
    const { type } = req.params;
    const log: any = req.body;
    const logGuardado = await this.logServicio.crearRegistro(
      type,
      log
    );
    if (logGuardado) {
      res.status(200).send({
        mensaje: "Registro de log exitoso"
      });
    } else {
      res.status(400).send({ mensaje: "Error en el registro de log" });
    }
  };
}
