import { Request, Response } from "express";
import { Inject, Service } from "typedi";
import UsuarioServicio from "../services/usuario/Usuario.service";
import UsuarioTransformador from "../transformers/Usuario.transformer";
import { IUsuario } from "../models/types/usuario/types";

@Service()
export default class UsuarioControlador {
  @Inject(type => UsuarioServicio)
  usuarioServicio: UsuarioServicio;
  @Inject(type => UsuarioTransformador)
  usuarioTransformador: UsuarioTransformador;

  public obtenerTodos = async (req: Request, res: Response) => {
    const { limit, page, filter } = req.query;
    try {
      const usuarios = await this.usuarioServicio.obtenerTodos(
        page,
        limit,
        filter
      );
      (usuarios as any).data = usuarios.data.map((usuarios: any) =>
        this.usuarioTransformador.transformador(usuarios)
      );
      res.status(200).send(usuarios);
    } catch (e) {
      res.status(400).send({ mensaje: "No se pudo consultar los usuario" });
    }
  };

  public crearUsuario = async (req: Request, res: Response) => {
    const usuario: IUsuario = req.body;
    const usuarioCreado = await this.usuarioServicio.crearUsuario(usuario);
    if (usuarioCreado) {
      const usuarioTransformado = this.usuarioTransformador.transformador(
        usuarioCreado
      );
      res.status(200).send({
        data: usuarioTransformado,
        mensaje: "Usuario guardado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "No se pudo guardar el usuario" });
    }
  };

  public actualizarUsuario = async (req: Request, res: Response) => {
    const { usuarioId } = req.params;
    const usuario: IUsuario = req.body;
    const usuarioGuardado = await this.usuarioServicio.actualizarUsuario(
      usuarioId,
      usuario
    );
    if (usuarioGuardado) {
      res.status(200).send({ mensaje: "Usuario actualizado exitosamente" });
    } else {
      res.status(400).send({ mensaje: "No se pudo actualizar el usuario" });
    }
  };

  public eliminarUsuario = async (req: Request, res: Response) => {
    const { usuarioId } = req.params;
    const username = req.body;
    const usuarioEliminado = await this.usuarioServicio.eliminarUsuario(
      usuarioId, username
    );
    if (usuarioEliminado) {
      res.status(200).send({ mensaje: "Usuario eliminado exitosamente" });
    } else {
      res.status(400).send({ mensaje: "No se pudo eliminar el usuario" });
    }
  };

  public consultaUsuario = async (req: Request, res: Response) => {
    const { usuarioId } = req.params;
    const usuarioConsulta = await this.usuarioServicio.consultaUsuario(
      usuarioId
    );
    if (usuarioConsulta) {
      const usuarioTransformado = this.usuarioTransformador.transformador(
        usuarioConsulta
      );
      res.status(200).send({
        data: usuarioTransformado,
        mensaje: "Usuario consultado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "Usuario consultado no existe" });
    }
  };

  public consultaUsuarioNombre = async (req: Request, res: Response) => {
    const params = req.body;
    const usuarioConsulta = await this.usuarioServicio.consultarUsuarioPorNombreAplicacion(
      params.username,
      params.codigoAplicacion
    );
    if (usuarioConsulta) {
      const usuarioTransformado = this.usuarioTransformador.transformador(
        usuarioConsulta
      );
      res.status(200).send({
        data: usuarioTransformado,
        mensaje: "Usuario consultado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "Usuario consultado no existe" });
    }
  };

  public consultaUsuarioLDAP = async (req: Request, res: Response) => {
    const { username } = req.params;
    const usuarioConsulta = await this.usuarioServicio.consultaUsuarioLDAP(
      username
    );
    if (usuarioConsulta) {
      res.status(200).send({
        data: usuarioConsulta,
        mensaje: "Usuario consultado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "Usuario consultado no existe" });
    }
  };

  public recuperarUsuario = async (req: Request, res: Response) => {
    const params = req.body;
    const usuarioConsultado = await this.usuarioServicio.recuperarUsuario(params);
    if (usuarioConsultado) {
      const usuarioTransformado = this.usuarioTransformador.transformadorRecuperarUsuario(
        usuarioConsultado
      );
      res.status(200).send({
        data: usuarioTransformado,
        mensaje: "Usuario consultado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "Correo no se encuentra registrado" });
    }
  };

  public resetearClave = async (req: Request, res: Response) => {
    const params = req.body;
    const usuarioConsultado = await this.usuarioServicio.resetarClave(params);
    console.log(usuarioConsultado);
    if (usuarioConsultado.success) {
      const usuarioTransformado = this.usuarioTransformador.transformadorRecuperarClave(
        usuarioConsultado
      );
      res.status(200).send({
        data: usuarioTransformado,
        mensaje: "Usuario consultado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "Error al resetear clave en el usuario" });
    }
  };

  public actualizarConexionUsuario = async (req: Request, res: Response) => {
    const { username } = req.params;
    const usuario: IUsuario = req.body;
    const usuarioGuardado = await this.usuarioServicio.actualizarConexionUsuario(
      username,
      usuario
    );
    if (usuarioGuardado) {
      res.status(200).send({ mensaje: "Usuario actualizado exitosamente" });
    } else {
      res.status(400).send({ mensaje: "No se pudo actualizar el usuario" });
    }
  };
  public actualizarValidacionScanReferenceUsuario = async (
    req: Request,
    res: Response
  ) => {
    const { username } = req.params;
    const usuario: IUsuario = req.body;
    const usuarioGuardado = await this.usuarioServicio.actualizarScanReferenceUsuario(
      username,
      usuario
    );
    if (usuarioGuardado) {
      res.status(200).send({ mensaje: "Usuario actualizado exitosamente" });
    } else {
      res.status(400).send({ mensaje: "No se pudo actualizar el usuario" });
    }
  };

  public actualizarValidacionFacialUsuario = async (
    req: Request,
    res: Response
  ) => {
    const { username } = req.params;
    const usuario: IUsuario = req.body;
    const usuarioGuardado = await this.usuarioServicio.actualizarValidacionFacialUsuario(
      username,
      usuario
    );
    if (usuarioGuardado) {
      res.status(200).send({ mensaje: "Usuario actualizado exitosamente" });
    } else {
      res.status(400).send({ mensaje: "No se pudo actualizar el usuario" });
    }
  };

  public consultaConexionUsuarioPorUsername = async (
    req: Request,
    res: Response
  ) => {
    const conexion: IUsuario = req.body;
    const usuarioConsulta = await this.usuarioServicio.consultarUsuarioPorNombreAplicacion(
      conexion.username,
      conexion.codigoAplicacion
    );
    if (usuarioConsulta) {
      const usuarioTransformado = this.usuarioTransformador.transformadorConexion(
        usuarioConsulta
      );
      res.status(200).send({
        data: usuarioTransformado,
        mensaje: "Usuario encontrado"
      });
    } else {
      res.status(400).send({ mensaje: "No se pudo encontrar el usuario" });
    }
  };

  public consultaUsuarioArrayIds = async (req: Request, res: Response) => {
    const params = req.body;
    const usuarioConsulta = await this.usuarioServicio.consultarUsuarioPorIds(
      params
    );
    if (usuarioConsulta.length > 0) {
      res.status(200).send({
        data: usuarioConsulta,
        mensaje: "Usuario consultado exitosamente"
      });
    } else {
      res.status(400).send({ mensaje: "Usuario consultado no existe" });
    }
  };

}
