import { Inject, Service } from "typedi";
import { Request, Response } from "express";
import WsoServicio from "../services/wso/Wso.service";

@Service()
export default class WsoControlador {
  @Inject(type => WsoServicio)
  wsoServicio: WsoServicio;

  public consultaWso = async (req: Request, res: Response) => {
    const wsoConsulta = await this.wsoServicio.consultaWso();
    if (wsoConsulta) {
      res.status(200).send({
        mensaje: "Registro de log exitoso"
      });
    } else {
      res.status(400).send({ mensaje: "Error en el registro de log" });
    }
  };
}
