import { Schema, model } from "mongoose";

export enum ESTADOSROL {
  ACTIVO = "ACTIVO",
  INACTIVO = "INACTIVO"
}

const rolEsquema = new Schema({
  id: {
    type: Number,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  nombre: {
    type: String,
    required: true
  },
  codigo: {
    type: String,
    required: true
  },
  reglas: {
    type: Array
  },
  codigoAplicacion: {
    type: String,
    required: true
  },
  tipo: {
    type: Number,
  },
  estado: {
    type: String,
    default: "ACTIVO"
  },
  fechaCreacion: {
    type: Date,
    default: Date.now()
  },
  fechaActualizacion: Date
});

export default model("Rol", rolEsquema);
