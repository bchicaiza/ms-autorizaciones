import { Document } from "mongoose";

export interface ICatalogo extends Document {
  codigo: string;
  nombre: string;
  descripcion: string;
  infoAdicional: object;
  codigoAplicacion: string,
  tipo: string;
  estado: string;
  fechaCreacion: Date;
  fechaActualizacion: Date;
  fechaEliminacion: Date;
}
