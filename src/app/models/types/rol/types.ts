import { Document } from "mongoose";

export interface IRol extends Document {
  nombre: string;
  codigo: string;
  reglas: any;
  codigoAplicacion: string,
  tipo: number,
  estado: string;
  fechaCreacion: Date;
  fechaActualizacion: Date;
  fechaEliminacion: Date;
}
