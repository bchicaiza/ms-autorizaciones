import axios from "axios";
import { Service } from "typedi";

@Service()
export class EncryptProvider {
  async Encrypt(data: any) {
    try {
      const host = process.env.ENCRYPT_RUTA as string;
      const url = host;
      const headers = {
        "Content-Type": "application/x-www-form-urlencoded"
      };
      const response = await axios.post(
        url,
        data,
        {
          headers
        }
      );
      return response.data;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
}
