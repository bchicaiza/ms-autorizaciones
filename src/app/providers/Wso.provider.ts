import axios from "axios";
import { Service } from "typedi";

@Service()
export class WsoProvider {
  async oauth2() {
    try {
      const host = process.env.AUTENTICACION_RUTA as string;
      // const url = `${host}/AutenticarUsuario`;
      const url = "http://GdyXywPVpT3h39ZBFmjzpxBOJlYa:zqtaZ0haVd2SWZ_p9Tk5sJtlLhQa@40.121.52.70:9763/oauth2/token?grant_type=password&username=prueba&password=prueba&scope=openid";
      const headers = {
        "Content-Type": "application/x-www-form-urlencoded"
      };
      const response = await axios.post(
        url,
        {},
        {
          headers
        }
      );
      return response.data;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
}
