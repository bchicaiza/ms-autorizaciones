import { Service } from "typedi";
import Usuario from "../models/Usuario";

const { map } = require("lodash");

@Service()
export default class UsuarioRepositorio {
  async obtenerTodos(filtros: any, _pagina: any, _limite: any) {
    const pagina = (_pagina - 1) * _limite;
    let whereQuery: any = { fechaEliminacion: { $exists: false } };
    try {
      const filtrosWhere = JSON.parse(filtros);
      map(filtrosWhere, (value: any, key: any) => {
        whereQuery[key] = { $eq: value + "" };
      });
    } catch (e) {
      whereQuery = { fechaEliminacion: { $exists: false } };
    }
    const datos = await Usuario.find(whereQuery)
      .skip(pagina)
      .limit(_limite);
    const cantidad = datos.length;
    const data = {
      data: datos,
      metadata: {
        total: cantidad,
        page: _pagina,
        limit: _limite
      }
    };
    return data;
  }

  async actualizarUsuario(id: string, usuario: any) {
    return await Usuario.updateOne({ _id: id }, usuario);
  }

  async eliminarUsuario(id: string) {
    return await Usuario.updateOne(
      { _id: id },
      { fechaEliminacion: new Date(), estado: "INACTIVO" }
    );
  }

  async usuarioVerificador(identificacion: any) {
    return await Usuario.find({
      identificacion: { $in: identificacion }
    });
  }

  async obtenerPorDocumentoUsuario(documento: string) {
    return await Usuario.findOne({
      identificacion: { $eq: documento }
    });
  }

  async consultarUsuarioId(id: string) {
    return await Usuario.findOne({
      _id: { $eq: id }
    });
  }

  async consultarUsuarioPorNombre(nombre: string, aplicacion: string) {
    return await Usuario.findOne({
      username: { $eq: nombre },
      codigoAplicacion: { $eq: aplicacion },
      estado: { $in: ["ACTIVO"] },
      fechaEliminacion: { $exists: false }
    }).populate("rol");
  }

  async consultarUsuarioPorCorreo(correo: string, aplicacion: string) {
    return await Usuario.findOne({
      email: { $eq: correo },
      codigoAplicacion: { $eq: aplicacion },
      estado: { $in: ["ACTIVO"] },
      fechaEliminacion: { $exists: false }
    }).populate("rol");
  }

  async actualizarUsuarioPorUsername(username: string, usuario: any) {
    return await Usuario.updateOne({ username: username }, usuario);
  }

  async consultarUsuarioPorIds(usuarios: any, aplicacion: string) {
    return await Usuario.find({
      _id: { $in: usuarios },
      codigoAplicacion: { $eq: aplicacion },
      estado: { $in: ["ACTIVO"] },
      fechaEliminacion: { $exists: false }
    }).populate("rol");
  }
}
