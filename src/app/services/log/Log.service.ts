import LogValidador from "../../validators/Log.validator";
import { Inject, Service } from "typedi";
import { errorLogger } from "../../../utils/logs/logs.utils";

@Service()
export default class LogServicio {
  @Inject(type => LogValidador)
  logValidador: LogValidador;

  async crearRegistro(type: string, log: any) {
    let registroLog = false;
    await this.logValidador.registro(log);
    try {
      switch (type) {
        case "error":
          errorLogger.error("Client Error", { log });
          break;
        case "warn":
          errorLogger.warning("Client Error", { log });
          break;
        case "info":
          errorLogger.info("Client Error", { log });
          break;
      }
      registroLog = true;
    } catch (e) {
      errorLogger.error("Client Error", { e });
    }
    return registroLog;
  }
}
