import UsuarioValidador from "../../validators/Usuario.validator";
import { Inject, Service } from "typedi";
import UsuarioRepositorio from "../../repositories/Usuario.repository";
import RolTransformador from "../../transformers/Rol.transformer";
import Usuario from "../../models/Usuario";
import ldap from "ldapjs";
import generator from "generate-password";
import { EncryptProvider } from "../../providers/Encrypt.provider";

@Service()
export default class UsuarioServicio {
  @Inject(type => UsuarioRepositorio)
  usuarioRepositorio: UsuarioRepositorio;
  @Inject(type => EncryptProvider)
  encryptProvider: EncryptProvider;
  @Inject(type => UsuarioValidador)
  usuarioValidador: UsuarioValidador;
  @Inject(type => RolTransformador)
  rolTransformador: RolTransformador;

  autenticateDN(username: string, password: string, datos: any) {
    const client = ldap.createClient({
      url: process.env.LDAP as string
    });

    client.bind(username, password, function(err) {
      if (err) {
        console.log("Error en la conexion");
      } else {
        console.log("Conexion Exitosa");
        const entry = {
          sn: datos.sn,
          cn: datos.cn,
          description: datos.description,
          mail: datos.mail,
          mobile: datos.mobile,
          o: datos.o,
          userPassword: datos.userPassword,
          givenName: datos.givenName,
          objectClass: [process.env.objectClass1 as string, process.env.objectClass2 as string, process.env.objectClass3 as string],
          pwdAttribute: process.env.pwdAttribute as string,
          pwdMaxAge: process.env.pwdMaxAge as string,
          pwdExpireWarning: process.env.pwdExpireWarning as string,
          pwdInHistory: process.env.pwdInHistory as string,
          pwdCheckQuality: process.env.pwdCheckQuality as string,
          pwdMinLength: process.env.pwdMinLength as string,
          pwdMaxFailure: process.env.pwdMaxFailure as string,
          pwdLockout: process.env.pwdLockout as string,
          pwdLockoutDuration: process.env.pwdLockoutDuration as string,
          pwdGraceAuthNLimit: process.env.pwdGraceAuthNLimit as string,
          pwdFailureCountInterval: process.env.pwdFailureCountInterval as string,
          pwdMustChange: process.env.pwdMustChange as string,
          pwdAllowUserChange: process.env.pwdAllowUserChange as string,
          pwdSafeModify: process.env.pwdSafeModify as string
        };
        const ou = process.env.OU_LDAP as string;

        const dc = process.env.DC_LDAP as string;
        const dc_secundario = process.env.DC_SECUNDARIO_LDAP as string;
        client.add(
          "uid=" +
          datos.uid +
          ",ou=" +
          ou +
          ",dc=" +
          dc +
          ",dc=" +
          dc_secundario,
          entry,
          err => {
            if (err) {
              console.log("error" + err);
            } else {
              console.log("Usuario Ingresado Exitosamente");
            }
          }
        );
      }
    });
  }

  updateDN(username: string, password: string, datos: any) {
    const client = ldap.createClient({
      url: process.env.LDAP as string
    });

    client.bind(username, password, function(err) {
      if (err) {
        console.log("Error en la conexion");
      } else {
        console.log("Conexion Exitosa");
        const ou = process.env.OU_LDAP as string;
        const dc = process.env.DC_LDAP as string;
        const dc_secundario = process.env.DC_SECUNDARIO_LDAP as string;
        const dn = "uid=" +
          datos.uid +
          ",ou=" +
          ou +
          ",dc=" +
          dc +
          ",dc=" +
          dc_secundario;
        const changeDescription = new ldap.Change({
          operation: "replace",
          modification: {
            description: datos.description
          }
        });
        const changeSn = new ldap.Change({
          operation: "replace",
          modification: {
            sn: datos.sn
          }
        });
        const changeCn = new ldap.Change({
          operation: "replace",
          modification: {
            cn: datos.cn
          }
        });
        const changeMail = new ldap.Change({
          operation: "replace",
          modification: {
            mail: datos.mail
          }
        });
        const changePwd = new ldap.Change({
          operation: "replace",
          modification: {
            pwdMustChange: "FALSE"
          }
        });
        // const changeMobile = new ldap.Change({
        //   operation: "replace",
        //   modification: {
        //     mobile: datos.mobile
        //   }
        // });
        const changeO = new ldap.Change({
          operation: "replace",
          modification: {
            o: datos.o
          }
        });
        const changeUserPassword = new ldap.Change({
          operation: "replace",
          modification: {
            userPassword: datos.userPassword
          }
        });
        const changeGivenName = new ldap.Change({
          operation: "replace",
          modification: {
            givenName: datos.givenName
          }
        });
        client.modify(dn, [changeDescription, changeCn, changePwd, changeSn, changeMail, changeO, changeUserPassword, changeGivenName], err => {
          if (err) {
            console.log("error" + err);
          } else {
            console.log("Usuario Actualizado Exitosamente");
          }
        });
      }
    });
  }

  deleteDn(username: string, password: string, datos: any) {
    const client = ldap.createClient({
      url: process.env.LDAP as string
    });

    client.bind(username, password, function(err) {
      if (err) {
        console.log("Error en la conexion");
      } else {
        console.log("Conexion Exitosa");
        const ou = process.env.OU_LDAP as string;
        const dc = process.env.DC_LDAP as string;
        const dc_secundario = process.env.DC_SECUNDARIO_LDAP as string;
        const dn = "uid=" +
          datos.uid +
          ",ou=" +
          ou +
          ",dc=" +
          dc +
          ",dc=" +
          dc_secundario;
        client.del(dn, err => {
          if (err) {
            console.log("error" + err);
          } else {
            console.log("Usuario Eliminado Exitosamente");
          }
        });
      }
    });
  }

  updateReseteoClave(username: string, password: string, datos: any) {
    return new Promise(function(resolve, reject) {
      const client = ldap.createClient({
        url: process.env.LDAP as string
      });
      client.bind(username, password, function(err) {
        if (err) {
          console.log("Error en la conexion");
        } else {
          console.log("Conexion Exitosa");
          const ou = process.env.OU_LDAP as string;
          const dc = process.env.DC_LDAP as string;
          const dc_secundario = process.env.DC_SECUNDARIO_LDAP as string;
          const dn = "uid=" +
            datos.uid +
            ",ou=" +
            ou +
            ",dc=" +
            dc +
            ",dc=" +
            dc_secundario;

          const changePwd = new ldap.Change({
            operation: "replace",
            modification: {
              pwdMustChange: "TRUE"
            }
          });
          const changeUserPassword = new ldap.Change({
            operation: "replace",
            modification: {
              userPassword: datos.userPassword
            }
          });
          client.modify(dn, [changePwd, changeUserPassword], err => {
            if (err) {
              reject({ success: false, data: err });
            } else {
              resolve({ success: true });
            }
          });
        }
      });
    });
  }

  async obtenerTodos(_page: any, _limit: any, filter: any) {
    const usuarios = await this.usuarioRepositorio.obtenerTodos(
      filter,
      parseInt(_page),
      parseInt(_limit)
    );
    return usuarios;
  }

  async crearUsuario(usuario: any) {
    await this.usuarioValidador.crearUsuario(usuario);
    let usuarioGuardado = null;
    const passwordGenerate = generator.generate({
      length: 8,
      numbers: true
    });
    // const obtenerContraseñaHash = await this.encryptProvider.Encrypt({
    //   message: passwordGenerate,
    //   password: process.env.passwordMS as string
    // });
    // if (obtenerContraseñaHash.data) {
    const consultaUsuario = await this.consultarUsuarioPorNombreAplicacion(usuario.username, usuario.codigoAplicacion);
    const consultaUsuarioCorreo = await this.consultarUsuarioPorAplicacionCorreo(usuario.email, usuario.codigoAplicacion);
    console.log(consultaUsuarioCorreo, consultaUsuario);
    if (!consultaUsuario && !consultaUsuarioCorreo) {
      usuario.password = passwordGenerate;
      const nuevoUsuario = new Usuario(usuario);
      usuarioGuardado = nuevoUsuario.save();
      if (usuarioGuardado) {
        try {
          const datosUsuarios = {
            sn: usuario.apellido,
            cn: usuario.nombre,
            mail: usuario.email,
            o: usuario.codigoAplicacion,
            description: usuario.identificacion,
            mobile: usuario.telefono,
            departmentNumber: usuario.cargo,
            userPassword: passwordGenerate,
            givenName: usuario.nombre + " " + usuario.apellido,
            uid: usuario.username
          };
          const cn = process.env.CN_LDAP as string;
          const password = process.env.PASSWORD_LDAP as string;
          const dc = process.env.DC_LDAP as string;
          const dc_secundaria = process.env.DC_SECUNDARIO_LDAP as string;
          this.autenticateDN(
            "cn=" + cn + ",dc=" + dc + ",dc=" + dc_secundaria,
            password,
            datosUsuarios
          );
        } catch (e) {
          console.log(e);
        }
        // }
      }
    }
    return usuarioGuardado;
  }

  async actualizarUsuario(id: string, usuario: any) {
    await this.usuarioValidador.actualizarUsuario(usuario);
    let usuarioGuardado = false;
    // const obtenerContraseñaHash = await this.encryptProvider.Encrypt({
    //   message: usuario.password,
    //   password: process.env.passwordMS as string
    // });
    // if (obtenerContraseñaHash.data) {
    try {
      const datosUsuarios = {
        sn: usuario.apellido,
        cn: usuario.nombre,
        mail: usuario.email,
        o: usuario.codigoAplicacion,
        description: usuario.identificacion,
        mobile: usuario.telefono,
        departmentNumber: usuario.cargo,
        userPassword: usuario.password,
        givenName: usuario.nombre + " " + usuario.apellido,
        uid: usuario.username
      };
      const cn = process.env.CN_LDAP as string;
      const password = process.env.PASSWORD_LDAP as string;
      const dc = process.env.DC_LDAP as string;
      const dc_secundaria = process.env.DC_SECUNDARIO_LDAP as string;
      this.updateDN(
        "cn=" + cn + ",dc=" + dc + ",dc=" + dc_secundaria,
        password,
        datosUsuarios
      );
    } catch (e) {
      console.log(e);
      return usuarioGuardado;
    }
    const consultaUsuario = await this.consultarUsuarioPorNombreAplicacion(usuario.username, usuario.codigoAplicacion);
    if (consultaUsuario && consultaUsuario._id == id) {
      const actualizacionUsuario = await this.usuarioRepositorio.actualizarUsuario(
        id,
        usuario
      );
      if (actualizacionUsuario) {
        if (actualizacionUsuario.n === 1) {
          usuarioGuardado = true;
        }
      }
      // }
    }
    return usuarioGuardado;
  }

  async eliminarUsuario(id: string, datos: any) {
    await this.usuarioValidador.eliminarUsuario(datos);
    let usuarioEliminado = false;
    try {
      const datosUsuarios = {
        uid: datos.username
      };
      const cn = process.env.CN_LDAP as string;
      const password = process.env.PASSWORD_LDAP as string;
      const dc = process.env.DC_LDAP as string;
      const dc_secundaria = process.env.DC_SECUNDARIO_LDAP as string;
      this.deleteDn(
        "cn=" + cn + ",dc=" + dc + ",dc=" + dc_secundaria,
        password,
        datosUsuarios
      );
    } catch (e) {
      console.log(e);
      return usuarioEliminado;
    }
    const eliminacionUsuario = await this.usuarioRepositorio.eliminarUsuario(
      id
    );
    if (eliminacionUsuario) {
      if (eliminacionUsuario.n === 1) {
        usuarioEliminado = true;
      }
    }
    return usuarioEliminado;
  }

  async crearMasivoUsuarios(usuarios: any) {
    const usuariosGuardados = await Usuario.create(usuarios);
    return usuariosGuardados;
  }

  async consultaUsuario(id: string) {
    const usuario = await this.usuarioRepositorio.consultarUsuarioId(id);
    return usuario;
  }

  async consultarUsuarioPorNombreAplicacion(nombre: string, aplicacion: string) {
    const obtenerUsuario = await this.usuarioRepositorio.consultarUsuarioPorNombre(
      nombre,
      aplicacion
    );
    return obtenerUsuario;
  }

  async consultarUsuarioPorAplicacionCorreo(correo: string, aplicacion: string) {
    const obtenerUsuario = await this.usuarioRepositorio.consultarUsuarioPorCorreo(
      correo,
      aplicacion
    );
    return obtenerUsuario;
  }

  async actualizarConexionUsuario(username: string, usuario: any) {
    await this.usuarioValidador.actualizarConexionUsuario(usuario);
    let usuarioGuardado = false;
    const actualizacionUsuario = await this.usuarioRepositorio.actualizarUsuarioPorUsername(
      username,
      usuario
    );
    if (actualizacionUsuario) {
      if (actualizacionUsuario.n === 1) {
        usuarioGuardado = true;
      }
    }
    return usuarioGuardado;
  }

  async actualizarScanReferenceUsuario(username: string, usuario: any) {
    await this.usuarioValidador.actualizarScanReferenceUsuario(usuario);
    let usuarioGuardado = false;
    const actualizacionUsuario = await this.usuarioRepositorio.actualizarUsuarioPorUsername(
      username,
      usuario
    );
    if (actualizacionUsuario) {
      if (actualizacionUsuario.n === 1) {
        usuarioGuardado = true;
      }
    }
    return usuarioGuardado;
  }

  async actualizarValidacionFacialUsuario(username: string, usuario: any) {
    await this.usuarioValidador.actualizarValidacionFacialUsuario(usuario);
    let usuarioGuardado = false;
    const actualizacionUsuario = await this.usuarioRepositorio.actualizarUsuarioPorUsername(
      username,
      usuario
    );
    if (actualizacionUsuario) {
      if (actualizacionUsuario.n === 1) {
        usuarioGuardado = true;
      }
    }
    return usuarioGuardado;
  }

  async consultaUsuarioLDAP(username: string) {
    return new Promise(function(resolve, reject) {
      const client = ldap.createClient({
        url: process.env.LDAP as string
      });
      const opts: any = {
        filter: "(|(uid=" + username + "))", // or search
        scope: "sub",
        attributes: ["sn"]
      };
      const ou = process.env.OU_LDAP as string;
      const dc = process.env.DC_LDAP as string;
      const dc_secundario = process.env.DC_SECUNDARIO_LDAP as string;
      const dn = "ou=" +
        ou +
        ",dc=" +
        dc +
        ",dc=" +
        dc_secundario;
      client.search(dn, opts, function(err: any, res: any) {
        if (err) {
          console.log("error" + err);
        } else {
          let query: any = null;
          res.on("searchEntry", function(entry: any) {
            query = entry.object;
          });
          res.on("searchEntry", function(entry: any) {
            resolve(query);
          });
        }
      });
    });
  }

  async recuperarUsuario(usuario: any) {
    await this.usuarioValidador.recuperarUsuario(usuario);
    const obtenerUsuario = await this.usuarioRepositorio.consultarUsuarioPorCorreo(
      usuario.correo,
      usuario.codigoAplicacion
    );
    return obtenerUsuario;
  }

  async resetarClave(usuario: any) {
    await this.usuarioValidador.resetearClave(usuario);
    const usuarioGuardado: any = {
      password: null,
      success: false
    };
    if (usuario.usuario) {
      const consultaUsuario: any = await this.consultarUsuarioPorNombreAplicacion(usuario.usuario, usuario.codigoAplicacion);
      if (consultaUsuario) {
        const passwordGenerate = generator.generate({
          length: 8,
          numbers: true
        });
        try {
          const datosUsuarios = {
            userPassword: passwordGenerate,
            uid: usuario.usuario
          };
          const cn = process.env.CN_LDAP as string;
          const password = process.env.PASSWORD_LDAP as string;
          const dc = process.env.DC_LDAP as string;
          const dc_secundaria = process.env.DC_SECUNDARIO_LDAP as string;
          const reseteo: any = await this.updateReseteoClave(
            "cn=" + cn + ",dc=" + dc + ",dc=" + dc_secundaria,
            password,
            datosUsuarios
          );
          if (reseteo.success) {
            consultaUsuario.password = passwordGenerate;
            const actualizacionUsuario = await this.usuarioRepositorio.actualizarUsuario(
              consultaUsuario._id,
              consultaUsuario
            );
            if (actualizacionUsuario) {
              if (actualizacionUsuario.n === 1) {
                usuarioGuardado.password = passwordGenerate;
                usuarioGuardado.success = true;
              }
            }
          }
        } catch (e) {
          console.log(e);
        }
      }
    } else {
      const usuarioRecuperado: any = await this.recuperarUsuario(usuario);
      if (usuarioRecuperado) {
        const consultaUsuario: any = await this.consultarUsuarioPorNombreAplicacion(usuarioRecuperado.username, usuario.codigoAplicacion);
        if (consultaUsuario) {
          const passwordGenerate = generator.generate({
            length: 8,
            numbers: true
          });
          try {
            const datosUsuarios = {
              userPassword: passwordGenerate,
              uid: consultaUsuario.username
            };
            const cn = process.env.CN_LDAP as string;
            const password = process.env.PASSWORD_LDAP as string;
            const dc = process.env.DC_LDAP as string;
            const dc_secundaria = process.env.DC_SECUNDARIO_LDAP as string;
            const reseteo: any = await this.updateReseteoClave(
              "cn=" + cn + ",dc=" + dc + ",dc=" + dc_secundaria,
              password,
              datosUsuarios
            );
            if (reseteo.success) {
              consultaUsuario.password = passwordGenerate;
              const actualizacionUsuario = await this.usuarioRepositorio.actualizarUsuario(
                consultaUsuario._id,
                consultaUsuario
              );
              if (actualizacionUsuario) {
                if (actualizacionUsuario.n === 1) {
                  usuarioGuardado.password = passwordGenerate;
                  usuarioGuardado.success = true;
                }
              }
            }
          } catch (e) {
            console.log(e);
          }
        }
      }
    }
    return usuarioGuardado;
  }

  async consultarUsuarioPorIds(params: any) {
    await this.usuarioValidador.consultarUsuariosIds(params);
    const obtenerUsuario = await this.usuarioRepositorio.consultarUsuarioPorIds(
      params.usuarios,
      params.codigoAplicacion
    );
    return obtenerUsuario;
  }
}
