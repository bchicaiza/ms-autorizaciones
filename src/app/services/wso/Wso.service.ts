import { Inject, Service } from "typedi";
import { WsoProvider } from "../../providers/Wso.provider";

@Service()
export default class WsoServicio {
  @Inject(type => WsoProvider)
  wsoProvider: WsoProvider;

  async consultaWso() {
    const registroLog = false;
    try {
      const oauth2 = await this.wsoProvider.oauth2();
      console.log(oauth2);
    } catch (e) {
      console.log(e);
    }
    return registroLog;
  }
}
