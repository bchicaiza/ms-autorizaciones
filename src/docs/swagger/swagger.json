{
  "swagger": "2.0",
  "info": {
    "title": "API's Autorizaciones Disponibles",
    "version": "1.0.0",
    "description": "Bienvenidos a la documentación aquí encontrará información para el uso correcto de cada una de las API's.\n\n## Cabezeras\n\n\nCada llamada a la API debe incluir los siguientes encabezados:\n\n```http\nContent-Type: application/json\n```\n\n## HTTP códigos\n\n\nLas API's utiliza códigos HTTP comunes para manejar solicitudes y respuestas..\nLa siguiente tabla muestra códigos HTTP comunes:\n\n| Code | Description                                                            |\n|------|------------------------------------------------------------------------|\n| 200  | Solicitud correcta.                                                      |\n| 400  | Se proporcionaron datos válidos pero la solicitud falló.                       |\n| 401  | No se proporcionó una clave de API válida.                                            |\n| 404  | No se pudo encontrar el recurso de solicitud.                               |\n| 405  | El método no está implementado                                          |\n| 422  | A la carga útil le faltan parámetros obligatorios o se proporcionaron datos no válidos. |\n| 429  | Demasiados intentos.                                                     |\n| 500  | La solicitud falló debido a un error interno.                               |\n| 503  | La API está fuera de línea por mantenimiento."
  },
  "host": "localhost:3031",
  "basePath": "/api/v1",
  "schemes": [
    "http",
    "https"
  ],
  "produces": [
    "application/json"
  ],
  "definitions": {
    "catalogo": {
      "type": "object",
      "properties": {
        "codigo": {
          "description": "Código de catalogo",
          "type": "string",
          "example": "Test"
        },
        "nombre": {
          "description": "nombre del catalogo",
          "type": "string",
          "example": "Test"
        },
        "descripcion": {
          "description": "Descripción del catalogo",
          "type": "string",
          "example": "Test"
        },
        "infoAdicional": {
          "description": "Información adicional del catálogo",
          "type": "object",
          "example": {
            "tipoDocumento": "RUC",
            "usuario": "1003520648",
            "endpointValidacion": "http://localhost:202"
          }
        },
        "tipo": {
          "description": "Tipo de catálogo",
          "type": "string",
          "example": "Test"
        },
        "codigoAplicacion": {
          "description": "Codigo de aplicación del catalogo en el cual va a ser utilizado",
          "type": "string",
          "example": "Test"
        },
        "estado": {
          "description": "Estado del catálogo",
          "type": "string",
          "example": "ACTIVO"
        },
        "fechaCreacion": {
          "description": "Fecha creación del catálogo",
          "type": "string",
          "example": "2020-01-01"
        },
        "fechaActualizacion": {
          "description": "Fecha actualización del catálogo",
          "type": "string",
          "example": "2020-01-01"
        },
        "fechaEliminacion": {
          "description": "Fecha eliminación del catálogo",
          "type": "string",
          "example": "2020-01-01"
        }
      },
      "required": [
        "codigo",
        "nombre",
        "infoAdicional",
        "tipo",
        "codigoAplicacion",
        "estado",
        "fechaCreacion"
      ]
    },
    "rol": {
      "type": "object",
      "properties": {
        "nombre": {
          "description": "Nombre de rol",
          "type": "string",
          "example": "Test"
        },
        "codigo": {
          "description": "Código de rol",
          "type": "string",
          "example": "Test"
        },
        "reglas": {
          "description": "Reglas de rol, arreglo de objetos o arreglos",
          "type": "array",
          "items": {
            "type": "object"
          }
        },
        "codigoAplicacion": {
          "description": "Código de aplicación para rol",
          "type": "string",
          "example": "RDC"
        },
        "tipo": {
          "description": "Tipo de rol",
          "type": "number",
          "example": 12345
        },
        "estado": {
          "description": "Estado de rol",
          "type": "string",
          "example": "ACTIVO"
        },
        "fechaCreacion": {
          "description": "Fecha de creación",
          "type": "string",
          "example": "2020-01-01"
        },
        "fechaActualizacion": {
          "description": "Fecha de actualización",
          "type": "string",
          "example": "2020-01-01"
        }
      },
      "required": [
        "codigo",
        "nombre",
        "codigoAplicacion"
      ]
    },
    "rolCatalogo": {
      "type": "object",
      "properties": {
        "id": {
          "description": "Id de rol",
          "type": "string",
          "example": "Test"
        },
        "nombre": {
          "description": "Nombre de rol",
          "type": "string",
          "example": "Test"
        },
        "tipo": {
          "description": "Tipo de rol",
          "type": "number",
          "example": 12345
        }
      },
      "required": [
        "id",
        "nombre",
        "tipo"
      ]
    }
  },
  "paths": {
    "/catalogo/APICAT001": {
      "get": {
        "tags": [
          "Catalogo"
        ],
        "summary": "Lista Catálogos",
        "responses": {
          "200": {
            "description": "Lista Catálogos",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/catalogo"
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "query",
            "name": "limit",
            "schema": {
              "type": "integer",
              "example": 10
            },
            "description": "Limite para mostrar en consulta"
          },
          {
            "in": "query",
            "name": "page",
            "schema": {
              "type": "integer",
              "example": 10
            },
            "description": "Página para la consulta"
          },
          {
            "in": "query",
            "name": "filter",
            "schema": {
              "type": "string",
              "example": "{codigoAplicacion:RDC,tipo:Identifcacion}"
            },
            "description": "Json de campos a filtrar en formato de string"
          }
        ]
      }
    },
    "/catalogo/APICAT002": {
      "post": {
        "tags": [
          "Catalogo"
        ],
        "summary": "Creación de catalogo",
        "responses": {
          "200": {
            "description": "Creación de catalogo",
            "schema": {
              "type": "object",
              "properties": {
                "mensaje": {
                  "type": "string",
                  "example": "Catálogo creado exitosamente"
                },
                "data": {
                  "$ref": "#/definitions/catalogo"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "body",
            "name": "Body",
            "schema": {
              "$ref": "#/definitions/catalogo"
            }
          }
        ]
      }
    },
    "/catalogo/APICAT003/{tipo}": {
      "get": {
        "tags": [
          "Catalogo"
        ],
        "summary": "Lista Catálogos",
        "responses": {
          "200": {
            "description": "Lista Catálogos",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/catalogo"
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "path",
            "name": "tipo",
            "description": "Tipo de catálogo",
            "schema": {
              "type": "string",
              "example": "ENTIDADFINANCIERA"
            }
          }
        ]
      }
    },
    "/catalogo/APICAT004/{catalogoId}": {
      "post": {
        "tags": [
          "Catalogo"
        ],
        "summary": "Actualización catálogo",
        "responses": {
          "200": {
            "description": "Actualización catálogo",
            "schema": {
              "type": "object",
              "properties": {
                "mensaje": {
                  "type": "string",
                  "example": "Catálogo actualizado exitosamente"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "body",
            "name": "Body",
            "schema": {
              "$ref": "#/definitions/catalogo"
            }
          },
          {
            "in": "path",
            "name": "catalogoId",
            "schema": {
              "type": "string",
              "example": "60c8c56000cff62d020a22db"
            }
          }
        ]
      }
    },
    "/rol/APIROL001": {
      "get": {
        "tags": [
          "Rol"
        ],
        "summary": "Lista roles",
        "responses": {
          "200": {
            "description": "Lista roles",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/rol"
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "query",
            "name": "limit",
            "schema": {
              "type": "integer",
              "example": 10
            },
            "description": "Limite para mostrar en consulta"
          },
          {
            "in": "query",
            "name": "page",
            "schema": {
              "type": "integer",
              "example": 10
            },
            "description": "Página para la consulta"
          },
          {
            "in": "query",
            "name": "filter",
            "schema": {
              "type": "string",
              "example": "{codigoAplicacion:RDC}"
            },
            "description": "Json de campos a filtrar en formato de string"
          }
        ]
      }
    },
    "/rol/APIROL002": {
      "post": {
        "tags": [
          "Rol"
        ],
        "summary": "Creación de rol",
        "responses": {
          "200": {
            "description": "Creación de rol",
            "schema": {
              "type": "object",
              "properties": {
                "mensaje": {
                  "type": "string",
                  "example": "Rol creado exitosamente"
                },
                "data": {
                  "$ref": "#/definitions/rol"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "body",
            "name": "Body",
            "schema": {
              "$ref": "#/definitions/rol"
            }
          }
        ]
      }
    },
    "/rol/APIROL003/{rolId}": {
      "post": {
        "tags": [
          "Rol"
        ],
        "summary": "Actualización rol",
        "responses": {
          "200": {
            "description": "Actualización rol",
            "schema": {
              "type": "object",
              "properties": {
                "mensaje": {
                  "type": "string",
                  "example": "Rol actualizado exitosamente"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "body",
            "name": "Body",
            "schema": {
              "$ref": "#/definitions/rol"
            }
          },
          {
            "in": "path",
            "name": "rolId",
            "schema": {
              "type": "string",
              "example": "60c8c56000cff62d020a22db"
            }
          }
        ]
      }
    },
    "/rol/APIROL004/{rolId}": {
      "post": {
        "tags": [
          "Rol"
        ],
        "summary": "Eliminar rol",
        "responses": {
          "200": {
            "description": "Eliminar rol",
            "schema": {
              "type": "object",
              "properties": {
                "mensaje": {
                  "type": "string",
                  "example": "Rol eliminado exitosamente"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "path",
            "name": "rolId",
            "schema": {
              "type": "string",
              "example": "60c8c56000cff62d020a22db"
            }
          }
        ]
      }
    },
    "/rol/APIROL005/{rolId}": {
      "get": {
        "tags": [
          "Rol"
        ],
        "summary": "Consulta rol",
        "responses": {
          "200": {
            "description": "Consulta rol",
            "schema": {
              "type": "object",
              "properties": {
                "mensaje": {
                  "type": "string",
                  "example": "Rol consultado exitosamente"
                },
                "data": {
                  "$ref": "#/definitions/rol"
                }
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "path",
            "name": "rolId",
            "schema": {
              "type": "string",
              "example": "60c8c56000cff62d020a22db"
            }
          }
        ]
      }
    },
    "/rol/APIROL006/catalogo/{aplicacion}": {
      "get": {
        "tags": [
          "Rol"
        ],
        "summary": "Consulta catalogo rol",
        "responses": {
          "200": {
            "description": "Consulta catalogo rol",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/rolCatalogo"
              }
            }
          },
          "400": {
            "description": "Bad request"
          },
          "500": {
            "description": "Internal Server Error"
          }
        },
        "parameters": [
          {
            "in": "path",
            "name": "aplicacion",
            "schema": {
              "type": "string",
              "example": "RDC"
            }
          }
        ]
      }
    }
  }
}
