import { Route, Methods } from "./routes.types";
import Container from "typedi";
import CatalogoControlador from "../app/controllers/Catalogo.controller";

const mainRoute = "/catalogo";

export const catalogoRutas: Route[] = [
  {
    path: `${mainRoute}/APICAT001`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(CatalogoControlador).obtenerTodos
  },
  {
    path: `${mainRoute}/APICAT002`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(CatalogoControlador).crearCatalogo
  },
  {
    path: `${mainRoute}/APICAT004/:catalogoId`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(CatalogoControlador).actualizarCatalogo
  },
  {
    path: `${mainRoute}/APICAT003/:tipo`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(CatalogoControlador).obtenerCatalogo
  }
];
