import { commonRoutes } from "./common.routes";
import { logRutas } from "./log.routes";
import { wsoRutas } from "./wso.routes";
import { usuarioRutas } from "./usuario.routes";
import { rolRutas } from "./rol.routes";
import { catalogoRutas } from "./catalogo.routes";

export default [
  ...commonRoutes,
  ...logRutas,
  ...wsoRutas,
  ...usuarioRutas,
  ...rolRutas,
  ...catalogoRutas
];
