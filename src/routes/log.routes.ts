import { Route, Methods } from "./routes.types";
import Container from "typedi";
import LogControlador from "../app/controllers/Log.controller";

const mainRoute = "/log";

export const logRutas: Route[] = [
  {
    path: `${mainRoute}/APILOG001/:type`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(LogControlador).crearRegistro
  },
];
