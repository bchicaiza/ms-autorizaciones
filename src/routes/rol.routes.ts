import { Route, Methods } from "./routes.types";
import Container from "typedi";
import RolControlador from "../app/controllers/Rol.controller";

const mainRoute = "/rol";

export const rolRutas: Route[] = [
  {
    path: `${mainRoute}/APIROL001`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(RolControlador).obtenerTodos
  },
  {
    path: `${mainRoute}/APIROL002`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(RolControlador).crearRol
  },
  {
    path: `${mainRoute}/APIROL003/:rolId`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(RolControlador).actualizarRol
  },
  {
    path: `${mainRoute}/APIROL004/:rolId`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(RolControlador).eliminarRol
  },
  {
    path: `${mainRoute}/APIROL005/:rolId`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(RolControlador).consultaRol
  },
  {
    path: `${mainRoute}/APIROL006/catalogo/:aplicacion`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(RolControlador).obtenerCatalogoRol
  }
];
