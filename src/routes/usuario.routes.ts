import { Route, Methods } from "./routes.types";
import Container from "typedi";
import UsuarioControlador from "../app/controllers/Usuario.controller";

const mainRoute = "/usuario";

export const usuarioRutas: Route[] = [
  {
    path: `${mainRoute}/APIUSU001`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(UsuarioControlador).obtenerTodos
  },
  {
    path: `${mainRoute}/APIUSU002/:usuarioId`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(UsuarioControlador).consultaUsuario
  },
  {
    path: `${mainRoute}/APIUSU003/:usuarioId`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).actualizarUsuario
  },
  {
    path: `${mainRoute}/APIUSU004`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).crearUsuario
  },
  {
    path: `${mainRoute}/APIUSU005/:usuarioId`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).eliminarUsuario
  },
  {
    path: `${mainRoute}/APIUSU006/:username`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(UsuarioControlador).consultaUsuarioLDAP
  },
  {
    path: `${mainRoute}/APIUSU007/consultaUsuario`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).consultaUsuarioNombre
  },
  {
    path: `${mainRoute}/APIUSU009`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).recuperarUsuario
  },
  {
    path: `${mainRoute}/APIUSU010`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).resetearClave
  },
  {
    path: `${mainRoute}/APIUSU011/:username`,
    method: Methods.PUT,
    middlewares: [],
    handler: Container.get(UsuarioControlador).actualizarConexionUsuario
  },
  {
    path: `${mainRoute}/APIUSU012/:username`,
    method: Methods.PUT,
    middlewares: [],
    handler: Container.get(UsuarioControlador)
      .actualizarValidacionScanReferenceUsuario
  },
  {
    path: `${mainRoute}/APIUSU013/:username`,
    method: Methods.PUT,
    middlewares: [],
    handler: Container.get(UsuarioControlador).actualizarValidacionFacialUsuario
  },
  {
    path: `${mainRoute}/APIUSU014`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador)
      .consultaConexionUsuarioPorUsername
  },
  {
    path: `${mainRoute}/APIUSU015`,
    method: Methods.POST,
    middlewares: [],
    handler: Container.get(UsuarioControlador).consultaUsuarioArrayIds
  },
];
