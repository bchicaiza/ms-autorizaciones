import { Route, Methods } from "./routes.types";
import Container from "typedi";
import WsoControlador from "../app/controllers/Wso.controller";

const mainRoute = "/wso";

export const wsoRutas: Route[] = [
  {
    path: `${mainRoute}/APIWSO001`,
    method: Methods.GET,
    middlewares: [],
    handler: Container.get(WsoControlador).consultaWso
  }
];
